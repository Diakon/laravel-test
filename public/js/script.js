let PROJECT = {
    init: function (settings) {
        // Конфиги
        PROJECT.config = {
            deleteProductClassBtn: '.js-order-products-row-delete',
            addProductClassBtn: '.js-order-products-row-add',
            cloneProductClassBlock: '.js-order-products-clone-block',
            productClassBlock: '.js-order-products-block',
            productsListPriceInputClass: '.js-product-price-input',
            ordersListTabClass: '.js-orders-tab',
        };

        $.extend(PROJECT.config, settings);

        PROJECT.ordersList();
        PROJECT.orderEdit();
        PROJECT.productEdit();
    },

    /**
     * Список заказов
     */
    ordersList: function () {
        // Нажали на конпку вкладки - тяну для нее данные, если уже не тянули ранее
        $(document).on('click', PROJECT.config.ordersListTabClass, function () {
            let tabId = $(this).attr('href');
            tabId = tabId.replace('#', '');
            if ($('p').hasClass('js-no-tab-data-' + tabId)) {
                // Для этой вкладки данные не тянули - тянем
                let url = $(this).data('url');
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    success: function( data ) {
                        console.log(data);
                        let html = data.content;
                        $('#' + tabId).empty().append(html);
                    },
                    error:  function(xhr, str){
                        alert('Ошибка при попытке получить список заказов');
                    }
                });
            }
        });
    },

    /**
     * Страница редактирования заказа
     */
    orderEdit: function () {
        // Нажали копку удаления позиции товара из заказа
        $(document).on('click', PROJECT.config.deleteProductClassBtn, function () {
            $(this).closest('.input-group ').remove();

            return false;
        });
        // Добавление новой позиции товара для заказа
        $(document).on('click', PROJECT.config.addProductClassBtn, function () {
            let pattern = $(PROJECT.config.cloneProductClassBlock).html();
            $(PROJECT.config.productClassBlock).append(pattern);

            return false;
        });
        // Для ввода кол-ва разрешаю только число.
        // Решаем через js, т.к. на форму мжно добавлять новые позициии товар / количество через кнопку +
        $(document).on('keyup', '.js-products-count-input', function () {
            let value = $(this).val().replace(/[^\d,]/g, '');
            $(this).val(value);
        });
    },

    /**
     * Редактирование товаров
     */
    productEdit: function () {
        // Разрешаме ввод только цифр
        $(document).on('keyup', PROJECT.config.productsListPriceInputClass, function () {
            let value = $(this).val().replace(/[^\d,]/g, '');
            $(this).val(value);
        });

        // Смена цена в списке товаров
        $(document).on('change', PROJECT.config.productsListPriceInputClass, function () {
            let price = parseInt($(this).val());
            let url = $(this).data('url');
            let token = $('#js-page-token').val();
            $.ajax({
                url: url,
                type: 'post',
                data: {
                    _token: token,
                    price: price,
                    url: url
                },
                dataType: 'json',
                success: function( data ) {
                    if (data.status == 'error') {
                        alert(data.msg)
                    }
                },
                error:  function(xhr, str){
                    alert('Ошибка сервера при обновлении цены!');
                }
            });
        });
    }
};
PROJECT.init();