<?php

namespace App\Modules;

class ModulesServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $modules = config("module.modules");
        if ($modules) {
            while (list(, $module) = each($modules)) {
                if (file_exists(__DIR__ . '/' . $module . '/Routes/routes.php')) {
                    $this->loadRoutesFrom(__DIR__ . '/' . $module . '/Routes/routes.php');
                }
                if (is_dir(__DIR__ . '/' . $module . '/Views')) {
                    $this->loadViewsFrom(__DIR__ . '/' . $module . '/Views', $module);
                }
                if (is_dir(__DIR__ . '/' . $module . '/Migration')) {
                    $this->loadMigrationsFrom(__DIR__ . '/' . $module . '/Migration');
                }
            }
        }
    }

    public function register()
    {

    }
}