<?php

namespace App\Modules\Orders\Models;

/**
 * Class PartnerRepository
 * @package App\Modules\Orders\Models
 */
class PartnerRepository extends Partner
{
    /**
     * Возращает запись по ID
     *
     * @param $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }
}