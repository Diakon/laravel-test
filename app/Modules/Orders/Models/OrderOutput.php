<?php

namespace App\Modules\Orders\Models;


class OrderOutput extends Order
{
    const STATUS_NEW = 0;
    const STATUS_CONFIRMED = 10;
    const STATUS_COMPLETED = 20;

    /**
     * @var array
     */
    public static $statusList = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_CONFIRMED => 'Потвержден',
        self::STATUS_COMPLETED => 'Завершен',
    ];
}