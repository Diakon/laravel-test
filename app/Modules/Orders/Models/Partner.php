<?php

namespace App\Modules\Orders\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Partner
 * @package App\Modules\Orders\Models
 */
class Partner extends Model
{
    protected $table = 'partners';

    protected $fillable = ['id', 'name', 'email'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'partner_id');
    }
}