<?php

namespace App\Modules\Orders\Models;

use App\Modules\Products\Models\ProductRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class OrderRepository
 * @package App\Modules\Orders\Models
 */
class OrderRepository extends Order
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $name;

    /**
     * @var array
     */
    public $productsIds = [];

    /**
     * @var array
     */
    public $productsCounts = [];

    /**
     * Возращает список заказов
     *
     * @param int $pagination
     * @return mixed
     */
    public static function getList($pagination = 25)
    {
        return self::orderBy('id', 'desc')->paginate($pagination);
    }

    /**
     * Возращает список просроченых заказов
     *
     * @param int $limit
     * @return mixed
     */
    public static function getListPastDue(int $limit = 50)
    {
        return self::where('delivery_dt', '<', Carbon::now())
            ->where('status', OrderOutput::STATUS_CONFIRMED)
            ->orderBy('delivery_dt', 'desc')
            ->limit($limit)
            ->get();
    }

    /**
     * Возращает список текущих заказов
     *
     * @return mixed
     */
    public static function getListCurrent()
    {
        $date = Carbon::now();
        $endDate = $date->addHours(24);

        return self::whereBetween('delivery_dt', [Carbon::now(), $endDate])
            ->where('status', OrderOutput::STATUS_CONFIRMED)
            ->orderBy('delivery_dt', 'asc')
            ->get();
    }

    /**
     * Возращает список новых заказов
     *
     * @param int $limit
     * @return mixed
     */
    public static function getListNew(int $limit = 50)
    {
        return self::where('delivery_dt', '>', Carbon::now())
            ->where('status', OrderOutput::STATUS_NEW)
            ->orderBy('delivery_dt', 'asc')
            ->limit($limit)
            ->get();
    }

    /**
     * Возращает список выполненных заказов
     *
     * @param int $limit
     * @return mixed
     */
    public static function getListCompleted(int $limit = 50)
    {
        $startDate = Carbon::today();
        $endDate = Carbon::tomorrow()->subSecond();

        return self::whereBetween('delivery_dt', [$startDate, $endDate])
            ->where('status', OrderOutput::STATUS_CONFIRMED)
            ->orderBy('delivery_dt', 'desc')
            ->limit($limit)
            ->get();
    }


    /**
     * Возращает запись по ID
     *
     * @param $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * Возращает сумму по заказу
     *
     * @return float|int
     */
    public function getTotalPrice()
    {
        $totalPrice = 0;
        foreach ($this->orderProducts as $orderProduct) {
            $totalPrice += $orderProduct->quantity * $orderProduct->price;
        }

        return $totalPrice;
    }

    public function saveByTransaction(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->client_email = $request->client_email;
            $this->status = $request->status;
            $this->save();
            // Удаляю товары в заказе,т.к. при редактировании могут добавить новы, удалить старые и тд.
            $deleteProducts = $this->orderProducts();
            $deleteProducts->delete();
            // Получаю цены на товары в заказе
            $prices = ProductRepository::getArrayByParams('id', 'price', 'id', $request->productsIds);
            // Обновляю записи в элементах заказа OrderProduct
            foreach ($request->productsIds as $key => $productId) {
                $orderProduct = new OrderProduct();
                $orderProduct->product_id = $productId;
                $orderProduct->quantity = $request->productsCounts[$key];
                $orderProduct->price = $prices[$productId];
                $this->orderProducts()->save($orderProduct);
            }
            // Обновляю email / имя у партнера
            $this->partner->email = $request->email;
            $this->partner->name = $request->name;
            $this->partner->save();

        } catch(\Exception $e)
        {
            DB::rollback();
            throw $e;
        }
        DB::commit();
    }
}