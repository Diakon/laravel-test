<?php

namespace App\Modules\Orders\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Orders\Models\OrderOutput;
use App\Modules\Orders\Models\OrderRepository;
use App\Modules\Products\Models\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Mail\Order;
use \Validator;
use \Response;

/**
 * Контроллер работы с заказами
 *
 * Class OrdersController
 * @package App\Http\Controllers
 */
class OrdersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Orders::index', [
            'orders' => $this->getListByType(),
        ]);
    }

    /**
     * Возращает список заказов по типу
     *
     * @param null $type
     * @return mixed
     */
    private function getListByType($type = null)
    {
        switch ($type) {
            // Просроченные
            case 'past':
                $list = OrderRepository::getListPastDue();
                break;
            // Текущие
            case 'current':
                $list = OrderRepository::getListCurrent();
                break;
            // Новые
            case 'new':
                $list = OrderRepository::getListNew();
                break;
            // Выполненные
            case 'completed':
                $list = OrderRepository::getListCompleted();
                break;
            default:
                $list = OrderRepository::getListPastDue();
        }

        return $list;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id)
    {
        $order = OrderRepository::getById($id);
        if (empty($order)) {
            abort(404);
        }

        return view('Orders::update', [
            'order' => $order,
            'productsList' => ProductRepository::getArray('id', 'name')
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(Request $request, $id)
    {
        $order = OrderRepository::getById($id);
        $validator = Validator::make($request->all(), [
            'client_email' => 'required|email|max:255',
            'email' => 'required|email|max:255|unique:partners,email,' . $order->partner_id,
            'name' => 'required|max:255',
            'status' => 'required|numeric',
            'productsIds.*' => 'required|numeric',
            'productsCounts.*' => 'required|numeric|min:1',
        ]);
        if ($validator->fails()) {
            return redirect(Redirect::back()->getTargetUrl())
                ->withErrors($validator)
                ->withInput();
        }
        // Сохраняю данные. Включаю транзакцию
        $order->saveByTransaction($request);
        // Если статус заказа указаои как "завершен" - отпрвлюя письма
        if ($order->status == OrderOutput::STATUS_COMPLETED) {
            // Отправляю письмо партнеру
            \Mail::to($order->partner)->send(new Order($order));
            // Отправляю письмо всем поставщикам
            foreach ($order->orderProducts as $orderProducts) {
                \Mail::to($orderProducts->product->vendor)->send(new Order($order));
            }
        }

        return redirect(Redirect::back()->getTargetUrl());
    }

    /**
     * Возращает HTML код страницы списка заказов по типу вкладки
     *
     * @param Request $request
     * @param $type
     * @return mixed
     * @throws \Throwable
     */
    public function getByType(Request $request, $type)
    {
        if (!$request->ajax()) {
            abort(404);
        }

        $content = view('Orders::_table', ['orders' => $this->getListByType($type)])->render();
        return Response::json([
            'content' => $content
        ]);
    }
}