<?php
/**
 * @var $orderProducts \App\Modules\Orders\Models\OrderProduct
 * @var $productsList[] array
 */
?>
@extends ('layouts.app')

@section('title')
    Редактирование заказа № {{ $order->id }}
@endsection

@section('content')

    <div class="js-order-products-clone-block">
        @include('Orders::_products', ['orderProducts' => new \App\Modules\Orders\Models\OrderProduct(), 'productsList' => $productsList])
    </div>

    <h2>Редактирование заказа № {{ $order->id }}</h2>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open([
        'route' => ['orders.store', $order->id],
        'class' => 'form-horizontal'
    ]) !!}

    <div class="popover-content">

        <div class="row">
            <div class="input-group">
                {!! Form::label('client_email', 'Email клиента:') !!}
                {!! Form::text('client_email', $order->client_email, ['placeholder' => 'Введите email клиента', 'class' => 'form-control', 'id' => 'client_email']) !!}
            </div>
            <div class="input-group">
                {!! Form::label('email', 'Email партнера:') !!}
                {!! Form::text('email', $order->partner->email, ['placeholder' => 'Введите email партнера', 'class' => 'form-control', 'id' => 'email']) !!}
            </div>
            <div class="input-group">
                {!! Form::label('name', 'Имя партнера:') !!}
                {!! Form::text('name', $order->partner->name, ['placeholder' => 'Введите имя партнера', 'class' => 'form-control', 'id' => 'name']) !!}
            </div>

            <div class="js-order-products-block">
                @foreach($order->orderProducts as $orderProducts)
                    @include('Orders::_products', ['orderProducts' => $orderProducts, 'productsList' => $productsList])
                @endforeach
            </div>
            <a href="#" class="btn btn-success js-order-products-row-add">+</a>

            <div class="input-group">
                {!! Form::label('status', 'Статус:') !!}
                {!! Form::select('status', \App\Modules\Orders\Models\OrderOutput::$statusList, $order->status, ['class' => 'form-control', 'id' => 'status']) !!}
            </div>

            <div class="input-group">
                <p><b>Сумма заказа: {{ $order->getTotalPrice()  }}</b></p>
            </div>

        </div>

    </div>



    <button type="submit" class="btn btn-success">Сохранить</button>
    <a href="{{ url("/orders")  }}" class="btn btn-danger">Отмена</a>
    {!! Form::close() !!}

@endsection