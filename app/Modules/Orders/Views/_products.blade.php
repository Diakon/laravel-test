<?php
/**
 * @var $orderProducts \App\Modules\Orders\Models\OrderProduct
 * @var $productsList[] array
 */
?>
<div class="input-group">
    <table>
        <thead>
            <tr>
                <th>
                    {!! Form::label('productsIds', 'Товар:') !!}
                </th>
                <th>
                    {!! Form::label('productsCounts', 'Количество:') !!}
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{!! Form::select('productsIds[]', $productsList, $orderProducts->product_id, ['class' => 'form-control']) !!}</td>
                <td>{!! Form::text('productsCounts[]', $orderProducts->quantity, ['placeholder' => 'Введите количество', 'class' => 'form-control js-products-count-input']) !!}</td>
                <td><a href="#" class="btn btn-danger js-order-products-row-delete">X</a></td>
            </tr>
        </tbody>
    </table>
</div>