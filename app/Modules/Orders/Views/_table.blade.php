<table class="table">
    <thead>
    <tr>
        <th scope="col">ID заказа</th>
        <th scope="col">Партнер</th>
        <th scope="col">Сумма заказа</th>
        <th scope="col">Состав</th>
        <th scope="col">Статус</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr>
            <th scope="row"><a href="{{ url("/orders/$order->id") }}">{{ $order->id }}</a></th>
            <td>{{ $order->partner->name }}</td>
            <td>{{ $order->getTotalPrice() }}</td>
            <td>
                <ul>
                    @foreach($order->orderProducts as $orderProduct)
                        <li>
                            Товар {{ $orderProduct->product->name ?? 'не указан' }} в количестве {{ $orderProduct->quantity ?? 'не указано' }} шт.
                        </li>
                    @endforeach
                </ul>
            </td>
            <td>{{ \App\Modules\Orders\Models\OrderOutput::$statusList[$order->status] ?? 'Статус не назначен'  }}</td>
        </tr>
    @endforeach
    </tbody>
</table>