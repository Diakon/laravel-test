@extends ('layouts.app')

@section('title')
    Список аказов
@endsection

@section('content')
    <input name="_token" id="js-page-token" type="hidden" value="{{ csrf_token() }}">

    <h2>Заказы:</h2>

    <div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#past"
                   aria-controls="past"
                   role="tab"
                   data-toggle="tab"
                >Просроченные</a>
            </li>
            <li role="presentation">
                <a href="#current"
                   aria-controls="current"
                   role="tab"
                   data-toggle="tab"
                   class="js-orders-tab"
                   data-url="{{ url("/orders/type/current") }}"
                >Текущие</a>
            </li>
            <li role="presentation">
                <a href="#new"
                   aria-controls="new"
                   role="tab"
                   data-toggle="tab"
                   class="js-orders-tab"
                   data-url="{{ url("/orders/type/new") }}"
                >Новые</a>
            </li>
            <li role="presentation">
                <a href="#completed"
                   aria-controls="completed"
                   role="tab" data-toggle="tab"
                   class="js-orders-tab"
                   data-url="{{ url("/orders/type/completed") }}"
                >Выполненные</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="past">
                @include('Orders::_table', ['orders' => $orders])
            </div>
            <div role="tabpanel" class="tab-pane" id="current">
                <p class="js-no-tab-data-current">Данные загружаются. Пожалуйста, подождите</p>
            </div>
            <div role="tabpanel" class="tab-pane" id="new">
                <p class="js-no-tab-data-new">Данные загружаются. Пожалуйста, подождите</p>
            </div>
            <div role="tabpanel" class="tab-pane" id="completed">
                <p class="js-no-tab-data-completed">Данные загружаются. Пожалуйста, подождите</p>
            </div>
        </div>
    </div>

@endsection
