<?php
Route::group(['namespace' => 'App\Modules\Orders\Controllers'], function () {
    Route::get('/orders', ['uses' => 'OrdersController@index']);
    Route::get('/orders/{id}', 'OrdersController@update');
    Route::post('/orders/store/{id}','OrdersController@store')->name('orders.store');
    Route::post('/orders/type/{type}', 'OrdersController@getByType');
});