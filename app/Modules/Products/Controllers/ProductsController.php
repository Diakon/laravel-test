<?php

namespace App\Modules\Products\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductRepository;
use Illuminate\Http\Request;
use \Validator;
use \Response;

/**
 * Контроллер вывода информации о продуктах
 *
 * Class ProductsController
 * @package App\Http\Controllers
 */
class ProductsController extends Controller
{
    public function index()
    {

        return view('Products::index', [
            'products' => ProductRepository::getList()
        ]);
    }

    /**
     * Ajax бновление цены для товара
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function setPrice(Request $request, $id)
    {
        if (!$request->ajax()) {
            abort(404);
        }
        $product = ProductRepository::getById($id);
        if (empty($product)) {
            return $this->returnJsonAnswer('Запись с таким ID не найдена!');
        }

        $validator = Validator::make($request->all(), [
            'price' => 'required|numeric|min:1',
        ]);
        if (!$validator->fails()) {
            $product->price = $request->price;
            if ($product->save()) {
                //ToDo тут, возможно, надо обновить стоимость товаров в заказах, но в ТЗ не указано,
                // а в мей практике было так, что это не надо делать. У пакупателя стоилмсть товара в заказах должна быть та, что он платил при покупке
                return $this->returnJsonAnswer('Цена для товара ' . $product->name . ' обновлена!', false);
            }
        }

        return $this->returnJsonAnswer('Ошибка сохранения цены для товара ' . $product->price);
    }

    /**
     * @param string $msg
     * @param bool $isError
     * @return mixed
     */
    private function returnJsonAnswer(string $msg, bool $isError = true)
    {
        return Response::json([
            'status' => $isError ? 'error' : 'success',
            'msg' => $msg
        ]);
    }
}