<?php

namespace App\Modules\Products\Models;

use App\Modules\Orders\Models\OrderProduct;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Modules\Products\Models
 */
class Product extends Model
{
    protected $table = 'products';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
}
