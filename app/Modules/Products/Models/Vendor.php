<?php

namespace App\Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'vendors';

    public function products()
    {
        return $this->hasMany(Product::class, 'vendor_id');
    }
}
