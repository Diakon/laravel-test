<?php

namespace App\Modules\Products\Models;


class ProductRepository extends Product
{
    /**
     * Возращает список заказов
     *
     * @param int $pagination
     * @return mixed
     */
    public static function getList(int $pagination = 25)
    {
        return self::orderBy('name', 'asc')->paginate($pagination);
    }

    /**
     * Возращает запись по ID
     *
     * @param $id
     * @return mixed
     */
    public static function getById(int $id)
    {
        return self::find($id);
    }

    /**
     * Возращает список
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    public static function getArray(string $key, string $value)
    {
        return self::pluck($value, $key);
    }

    /**
     * Возращает список по параметрам
     *
     * @param string $key
     * @param string $value
     * @param string $keyParams
     * @param array $params
     * @return mixed
     */
    public static function getArrayByParams(string $key, string $value, string  $keyParams, array $params = [])
    {
        return self::whereIn($keyParams, $params)->pluck($value, $key);
    }
}