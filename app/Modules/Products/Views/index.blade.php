@extends ('layouts.app')

@section('title')
    Список товаров
@endsection

@section('content')

    <h2>Товары:</h2>

    <input name="_token" id="js-page-token" type="hidden" value="{{ csrf_token() }}">

    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID товара</th>
            <th scope="col">Наименование товара</th>
            <th scope="col">Наименование поставщика</th>
            <th scope="col">Цена</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <th scope="row">{{ $product->id }}</th>
                <td>{{ $product->name }}</td>
                <td>
                    Имя {{ $product->vendor->name ?? 'не указано' }}, email: {{ $product->vendor->email ?? 'не указано' }}
                </td>
                <td>
                    <input
                            type="text"
                            value="{{ $product->price }}"
                            data-url="{{ url("/products/set-price", ['id' => $product->id]) }}"
                            class="form-control js-product-price-input"
                    >
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <?php echo $products->render(); ?>

@endsection
