<?php
Route::group(['namespace' => 'App\Modules\Products\Controllers'], function () {
    Route::get('/products', ['uses' => 'ProductsController@index']);
    Route::post('/products/set-price/{id}', ['uses' => 'ProductsController@setPrice']);
});