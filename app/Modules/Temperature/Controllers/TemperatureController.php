<?php

namespace App\Modules\Temperature\Controllers;

use App\Http\Controllers\Controller;

/**
 * Контроллер вывода информации о температуре
 *
 * Class TemperatureController
 * @package App\Http\Controllers
 */
class TemperatureController extends Controller
{
    use YandexTrait;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Temperature::index', [
            'weather' => $this->getWeatherYandex(),
        ]);
    }
}