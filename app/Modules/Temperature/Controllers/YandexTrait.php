<?php
namespace App\Modules\Temperature\Controllers;

use Illuminate\Support\Facades\Cache;

/**
 * Трейт для работы с погодой Яндекса
 *
 * Trait YandexWeatherTrait
 * @package App\Modules\Temperature\Controllers
 */
trait YandexTrait
{
    /**
     * @var string Ссылка на Яндекс Погоду
     */
    private $url = 'https://api.weather.yandex.ru/v1/forecast';
    /**
     * @var string Ключ АПИ Яндекса
     */
    private $apiKey = '12303777-ad3d-4326-a2ed-d8b62d90e40d';

    /**
     * Возращает погоду в Яндексе
     *
     * @param float $lat
     * @param float $lon
     * @return mixed
     */
    public function getWeatherYandex(float $lat = 53.25209, float $lon = 34.37167)
    {
        // Запрос на получение погоды кешируем
        $cacheKey = 'yandexWeatherLat' . $lat . 'Lon' . $lon;
        $content = Cache::get($cacheKey);
        if (empty($content)) {
            $this->url .= "?lat=$lat&lon=$lon";
            $client = new \GuzzleHttp\Client();
            $response = $client->get($this->url, [
                'headers' => ['X-Yandex-API-Key' => $this->apiKey],
            ]);
            $content = $response->getBody()->getContents();

            // Время жизни кеша 10 минут
            Cache::add($cacheKey, $content, 10 * 60);
        }

        $content = json_decode($content, true);

        return $content['fact'];
    }
}
