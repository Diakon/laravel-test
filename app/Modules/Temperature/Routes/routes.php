<?php
Route::group(['namespace' => 'App\Modules\Temperature\Controllers',
    'as' => 'temperature',
], function () {
    Route::get('/temperature', ['uses' => 'TemperatureController@index']);
});