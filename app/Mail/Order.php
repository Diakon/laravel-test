<?php

namespace App\Mail;

use App\Modules\Orders\Models\OrderRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Order extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var OrderRepository
     */
    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OrderRepository $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.order');
    }
}
