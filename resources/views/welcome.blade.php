<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="m-b-md">
                    Тестовое задание Скляров Петра<br>
                    В реализации применялось кеширование (для получения температуры).<br>
                    Транзакции (для страницы редактирования заказа).<br>
                    Ajax и js для добавления/удаления товаров в заказе.<br>
                    Ajax изменение цены и пагинация в списке товаров.<br>
                    Отправка писем поставщику и партнеру при сохранении заказа в статусе "завершен".<br>
                    Вкладки и использование Ajax при переключении вкладок в списке заказов.<br>
                </div>
                <hr>
                <div class="links">
                    <a href="{{ url("/temperature") }}">Температура в Брянске</a>
                    <a href="{{ url("/orders") }}">Заказы</a>
                    <a href="{{ url("/products") }}">Товары</a>
                </div>
            </div>
        </div>
    </body>
</html>
