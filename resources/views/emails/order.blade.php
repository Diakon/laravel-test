@component('mail::message')

    Заказ № {{ $order->id }} номер-заказа переведн в статус "завершен"
    Состав закза:
    <ul>
        @foreach($order->orderProducts as $orderProduct)
            <li>
                Товар {{ $orderProduct->product->name ?? 'не указан' }} в количестве {{ $orderProduct->quantity ?? 'не указано' }} шт.
            </li>
        @endforeach
    </ul>

    Сумма заказа: {{ $order->getTotalPrice() }}

    С увжением,
    {{ config('app.name') }}

@endcomponent