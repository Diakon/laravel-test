<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbar-main">
            <ul class="nav navbar-nav">
                <li><a class="icoEl_client" href="{{ url('/temperature') }}"><span>Температура</span></a></li>
                <li><a href="{{ url('/orders') }}">Заказы</a></li>
                <li><a href="{{ url('/products') }}">Продукты</a></li>
            </ul>
        </div>
    </div>
</nav>